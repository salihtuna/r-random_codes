rownames(rawCounts) <- paste(rawCounts$gene_id, rawCounts$gene_name, sep = "_")
rawCounts[c('gene_id', 'gene_name')] <- NULL
rawCounts <- rawCounts[rowSums(rawCounts) > 5,]
cdmR <- as.matrix(rawCounts)
mode(cdmR) <- "integer" 

#batch <- sampleData$batch
#batch <- as.numeric(sampleData$batchN)
#adjusted <- ComBat_seq(rawCounts, batch=batch, group=NULL)
#adjusted <- ComBat_seq(cdmR, batch=batch, group=NULL)