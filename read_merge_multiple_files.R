project_path <- "C:/Users/SalihTuna/Projects/RNA-Seq/results_dir"

rawCount_files <- list.files(file.path(project_path), pattern = "*gene_counts.tsv", recursive = T, full.names = T)
df_neo <- map(rawCount_files, read.delim)
rawCounts <- reduce(df_neo, by = c('gene_id','gene_name'), full_join) # left_join